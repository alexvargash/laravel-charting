<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('wins', function() {
    $jeffrey = collect(['name' => 'Jaffrey', 'wins' => 50]);
    $taylor = collect(['name' => 'Taylor', 'wins' => 8]);
    $alex = collect(['name' => 'Alejandro', 'wins' => 120]);
    $santi = collect(['name' => 'Santiago', 'wins' => 150]);
    return view('wins', compact('jeffrey', 'taylor', 'alex', 'santi'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
