@extends('layouts.app')

@section('content')
<div id="root" class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Rocket League Wins Comparison</h5>

                    <wins-graph :player="{{ $jeffrey }}" :opponent="{{ $taylor }}" ></wins-graph>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Rocket League Wins Comparison</h5>

                    <wins-graph :player="{{ $alex }}" :opponent="{{ $santi }}" ></wins-graph>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/wins.js') }}?1001"></script>
@endsection
