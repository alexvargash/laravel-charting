@extends('layouts.app')

@section('content')
<div id="root" class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">

                    <graph :labels="['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun']" 
                      	   :values="[30, 20, 80, 104, 90, 55]"
                    ></graph>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/main.js') }}?1005"></script>
@endsection
