import Vue from 'vue';
import WinsGraph from './components/WinsGraph';

new Vue({
    el: '#root',

    components: { WinsGraph }
});