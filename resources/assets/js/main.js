import Vue from 'vue';
import Graph from './components/Graph'

new Vue({
	el: '#root',
	components: { Graph }
});