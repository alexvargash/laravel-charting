(function() {
    var data = {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
        datasets: [
            {
                data: [30, 20, 80, 104, 90, 55],
                label: "Africa",
                borderColor: "#3e95cd",
                fill: false
            },
            {
                data: [50, 60, 10, 40, 45, 90],
                label: "America",
                borderColor: "#3e6600",
                fill: false
            }
        ] 
    };
    
    var context = document.getElementById('graph');
       
    var myLineChart = new Chart(context, {
        type: 'line',
        data: data,
        options: {
            title: {
                display: true,
                text: 'World population per region (in millions)'
            }
        }
    });

})();